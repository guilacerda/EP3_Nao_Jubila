# EP3 - OO (UnB - Gama)

Esse projeto consiste em uma aplicação web que proporciona o compartilhamento de conhecimento entre pessoas.

## Como Executar

Para executar o programa em um sistema operacional Linux, siga as seguintes instruções:

* Abra o Terminal 
* Encontre o diretório raiz do projeto
* Baixe as gems usadas no projeto:
	**$ bundle install** 
* Rode as migrações:
    **$ rake db:migrate**
* Inicialize o servidor:
	**$ rails s**
* Abra um navegador de sua escolha
* Acessa o servidor local digitando : localhost:3000

##Descrição de versões (Desenvolvimento)

* Ruby version : ruby 2.3.0p0

* Rails version : Rails 5.0.0.1

##Funcionamento

* O usuário tem a opção de se cadastrar ou não, de logar ou não.
* Não cadastrado : Apenas a visualização dos posts e nas demais páginas do site disponiveis inicialmente
* O usuario cadastrado poderá:
* Trocar a senha quando quiser
* Criar posts
* Editar seus posts
* Excluir seus posts
